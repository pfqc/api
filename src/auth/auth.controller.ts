import { Controller, Get, UseGuards, Body, Post, Session, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { CreateTokenDto } from './dto/create-token.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post()
  @UseGuards(AuthGuard('local'))
  async create(@Body() payload: CreateTokenDto) {
    return this.authService.createToken(payload.email);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async get(@Req() req) {
    return req.user;
  }
}
