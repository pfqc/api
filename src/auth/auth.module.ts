import { Module, forwardRef } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';
import { AuthController } from './auth.controller';
import { Constants } from '../constants';
import { ConfigService } from '../config/config.service';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';

const bcryptLibProvider = {
  provide: Constants.BcryptLibProvider,
  useValue: bcrypt,
};

const jwtLibProvider = {
  provide: Constants.JwtLibProvider,
  useValue: jwt,
};

const jwtSecretKeyProvider = {
  provide: Constants.JwtSecretKeyProvider,
  useFactory: (configService: ConfigService) => configService.secretKey,
  inject: [ConfigService],
};

const jwtTimeoutProvider = {
  provide: Constants.JwtTimeoutProvider,
  useFactory: (configService: ConfigService) => configService.authTimeout,
  inject: [ConfigService],
};

@Module({
  imports: [forwardRef(() => UserModule)],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy, jwtLibProvider, jwtSecretKeyProvider, jwtTimeoutProvider, bcryptLibProvider],
  exports: [AuthService],
})
export class AuthModule { }
