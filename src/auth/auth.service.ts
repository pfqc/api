import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { Constants } from '../constants';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    @Inject(Constants.BcryptLibProvider) private readonly bcrypt,
    @Inject(Constants.JwtLibProvider) private readonly jwt,
    @Inject(Constants.JwtTimeoutProvider) private readonly timeout: number,
    @Inject(Constants.JwtSecretKeyProvider) private readonly secret: number) { }

  async createToken(email: string) {
    const token: JwtPayload = { email };
    const accessToken = this.jwt.sign(token, this.secret, { expiresIn: this.timeout });

    return {
      expiresIn: this.timeout,
      accessToken,
    };
  }

  async validateEmail(payload: JwtPayload) {
    return await this.userService.findOneByEmail(payload.email);
  }

  async validateCreds(email: string, password: string) {
    const user = await this.userService.findOneByEmail(email);

    if (!user) {
      return false;
    }

    const valid = await this.bcrypt.compare(password, user.hash);

    if (valid) {
      return user;
    }

    return false;
  }

  async createHash(password: string): Promise<string> {
    const salt = await this.bcrypt.genSalt();
    return await this.bcrypt.hash(password, salt);
  }
}
