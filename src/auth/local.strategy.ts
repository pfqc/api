import { Strategy } from 'passport-local';
import { AuthService } from './auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '../user/user.entity';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email',
    });
  }

  async validate(email: string, password: string, done: (error: Error, auth: false | User) => any) {
    const user = await this.authService.validateCreds(email, password);

    if (!user) {
      return done(new UnauthorizedException(), false);
    }

    return done(null, user);
  }
}
