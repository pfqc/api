export enum Constants {
  BcryptLibProvider = 'BcryptProvider',
  JwtLibProvider = 'JwtProvider',
  JwtTimeoutProvider = 'JwtTimeoutProvider',
  JwtSecretKeyProvider = 'JwtSecretKeyProvider',
}
