import { Controller, UseGuards, Get, Post } from '@nestjs/common';
import { ProjectService } from './project.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateProjectDto } from './dto/create-project.dto';

@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) { }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async findAll() {
    return this.projectService.findAll();
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(payload: CreateProjectDto) {
    return this.projectService.create(payload);
  }
}
