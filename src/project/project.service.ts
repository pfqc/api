import { Injectable } from '@nestjs/common';
import { Project } from './project.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProjectDto } from './dto/create-project.dto';

@Injectable()
export class ProjectService {
  constructor(@InjectRepository(Project)
  private readonly projectRepository: Repository<Project>) { }

  async findAll() {
    return this.projectRepository.find();
  }

  async findById(id: number) {
    return this.projectRepository.findOne(id);
  }

  async findBy(props: Partial<Project>) {
    return this.projectRepository.find(props);
  }

  async create(project: CreateProjectDto) {

  }
}
