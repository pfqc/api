import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Project } from '../project/project.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  hash: string;

  @OneToMany(type => Project, project => project.user) // note: we will create author property in the Photo class below
  projects: Project[];
}
