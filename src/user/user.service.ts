import { Injectable, Inject, HttpException, HttpStatus, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
  ) { }

  async findOneByEmail(email: string) {
    return await this.userRepository.findOne({ email });
  }

  async create(email: string, password: string) {
    const exists = await this.findOneByEmail(email);

    if (exists) {
      return new HttpException('Email already exists', HttpStatus.BAD_REQUEST);
    }

    const hash = await this.authService.createHash(password);

    const user = this.userRepository.create({ email, hash });
    await this.userRepository.save(user);
    return this.authService.createToken(email);
  }
}
